<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Index Page</title>
		<link rel="stylesheet" href="{{asset('css/app.css')}}">
	</head>
	<body>
		<div class="container">
		<br />
		@if (\Session::has('success'))
		<div class="alert alert-success">
			<p>{{ \Session::get('success') }}</p>
		</div><br />
		@endif
		@if ($jenisSim == 1)
		<p>Movie yang direkomendasikan untuk user {{$idUser}} , jenis similarity : vektor </p>
		@else
		<p>Movie yang direkomendasikan untuk user {{$idUser}} , jenis similarity : biner </p>
		@endif
		<table class="table table-striped">
			<thead>
				<tr>
					<th>MovieId</th>
					<th>Rating</th>
				</tr>
			</thead>
			<tbody>
				@foreach($rating_prediksi as $rating)
				<tr>
					<td>{{$rating['name']}}</td>
					<td>{{$rating['rating']}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</body>
</html>
