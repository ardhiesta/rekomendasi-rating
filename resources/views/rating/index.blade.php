<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Index Page</title>
		<link rel="stylesheet" href="{{asset('css/app.css')}}">
	</head>
	<body>
		<div class="container">
            <div class="row text-center ">
                <div class="col-md-12">
                    <br /><br />
                    <h2> Check Your Movie Recomendation!</h2>

                    <h5></h5>
                    <br />
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>ID User: </strong>  
                        </div>
                        <div class="panel-body">
                            <form method="post" action="{{url('ratingprediksi')}}">
                            {{csrf_field()}}
                                <!-- <input name="_method" type="hidden" value="PATCH"> -->
                                <br />
                                <div class="form-group input-group">
                                <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                <input type="text" class="form-control" name="userid" />
                                </div>
                                <button class="btn btn-danger" name="submit">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>